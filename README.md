![GIF_2](example.gif)
# Fish Midline

Extract the posture midline of animals (mainly fish) tracked with [idtracker.ai](https://idtracker.ai).

------------

The pipeline only requires the `list_of_blobs.pickle` generated after tracking the video.

From the binary mask of a idtrackerai `Blob`, scikit-image's [skeletonize](https://scikit-image.org/docs/stable/api/skimage.morphology.html#skimage.morphology.skeletonize) is used to extract the skeleton. Then, the head of the animal is extracted from looking at the 2nd local maxima of blob's contour curvature. A spline is fitted following the skeleton and starting at the head of the animal.

This is not a finished project but an example of how to use blob's data and extract posture information. Because of this, this repository is not a package, it's not installable, you may want to clone it and modify it to your necessities.

A `usage_example.ipynb` is provided as an example of how to use the main module `fishmidline.py`, where `get_spline` is the main function.

## Requirements
* idtrackerai >= 5.2.0
* scikit-image

## Contributors
- Jordi Torrents
- Francisco Romero-Ferrero (francisco.romero@neuro.fchampalimaud.org)
